%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%assign MAX_BUF_SIZE 255

section .bss
input: resb MAX_BUF_SIZE db 0
section .rodata
error: db 'Error', 0

section .text
global _start

_start:
	mov rdi, input
	mov rsi, MAX_BUF_SIZE
	call read_string
	test rax, rax
	je .error
	mov rdi, rax
	mov rsi, first_word 
	call find_word
	test rax, rax
	je .error
	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	.end:
		call exit
	.error:
		mov rdi, error
		call print_string_error
		call exit
