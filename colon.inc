%define next 0 
%macro colon 2
	%ifid %2
	%else
		%error "Неудачное название метки"
	%endif
	%ifstr %1
	%else
		%error "Неудачное значение ключа"
	%endif
	%2:
	dq next
	db %1, 0
%define next %2
%endmacro
