global find_word
%include "lib.inc"
%define eight 8
section .text

find_word:
	;rdi - 0-терминированная строка
	;rsi - указатель на начало словаря
	.loop:
		test rsi, rsi
		je .unsuccess
		push rsi
		push [rsi]
		add rsi, eight
		push rdi
		call string_equals
		pop rdi
		cmp rax, 1
		je .success
		pop rsi
		jmp .loop
	.success:
		pop rax
		ret
	.unsuccess:
		mov rax, 0
		ret
			
